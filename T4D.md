
# T4D Seite

Hier verwende ich Markdown Syntax. Wird der Inhaltsbereich breiter, wenn ich mehr Text eingebe? Momentan ist die rechte Sidebar viel zu breit und nimmt zu viel Platz sein in der Desktopversion.

## Versicherungen

### Übersicht

|                  | SWICA       | HELVETIA | BALOISE | MOBILIAR | AXA |
| -----------      | ----------- | -------- |-------- |--------- |---- |
| UVG              | Title       |          |         |          |     |
| UVG Zusatz       | Text        |          |         |          |     |
| BVG vollver.     | Text        |          |         |          |     |
| UVG teil-autonom | Text        |          |         |          |     |

### Unfallversicherungen (UVG)

1. SWICA
2. Baloise
3. Helvetia
4. Mobiliar
5. AXA

### Unfallversicherung Zusatz

### Pensionskasse (BVG)

1. sage pensionskassse


## Links

https://t4d.ch
