.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Erstes Projekt
==============

.. toctree::
   :maxdepth: 2   
   :caption: Contents:
   
   T4D.md


Inhalt und Suche
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
